package com.example.app1


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        findViewById<Button>(R.id.button).setOnClickListener{
            var intent = Intent(this,helloActivity::class.java)
            var name = binding.textView2.text.toString()
            intent.putExtra("name",name)
            this.startActivity(intent)
        }
    }
}



